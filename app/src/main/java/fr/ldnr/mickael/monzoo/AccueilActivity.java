package fr.ldnr.mickael.monzoo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.prefs.Preferences;

public class AccueilActivity extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil);
        lireNouvelles();
        Button btCarte = findViewById(R.id.btCarte);
        btCarte.setOnClickListener(this);
        Button btAlerte = findViewById(R.id.btAlerte);
        btAlerte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AccueilActivity.this,
                        AlerteActivity.class);
                startActivity(i);
            }
        });
        Button btAnnuaire = findViewById(R.id.btAnnuaire);
        btAnnuaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AccueilActivity.this,
                        AnnuaireActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, CarteActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.accueil, menu);
        SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
        menu.findItem(R.id.menu_envoyer).setChecked(
                sp.getBoolean("envoyer", true));
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, @NonNull MenuItem item) {
        if(item.getItemId()==R.id.menu_carte) {
            startActivity(new Intent(this, CarteActivity.class));
        }
        if(item.getItemId()==R.id.menu_animal) {
            startActivity(new Intent(this, AnimalActivity.class));
        }
        if(item.getItemId()==R.id.menu_envoyer) {
           item.setChecked(!item.isChecked());

            SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("envoyer", item.isChecked());

            e.apply();
        }
        return true;
    }

    private void lireNouvelles() {
        try {
            InputStream is = getAssets().open("news.txt");
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(is, StandardCharsets.UTF_8));
            String ligne, contenu = "";
            while( (ligne=br.readLine()) != null) {
                contenu += ligne + "\n";
            }
            TextView tvNouvelles = findViewById(R.id.tvNouvelles);
            tvNouvelles.setText(contenu);
        } catch(Exception ex) {
            Log.e("AccueilActivity", "Erreur lecture news", ex);
        }
    }


}
