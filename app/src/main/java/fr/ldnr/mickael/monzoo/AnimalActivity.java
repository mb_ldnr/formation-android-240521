package fr.ldnr.mickael.monzoo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AnimalActivity extends Activity implements View.OnClickListener {

    private Handler mainHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal);
        Button btEnvoi = findViewById(R.id.btEnvoi);
        btEnvoi.setOnClickListener(this);
        Button btChercher = findViewById(R.id.btChercher);
        btChercher.setOnClickListener(this);
        mainHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onClick(View view) {
        EditText etNom = findViewById(R.id.etNom);
        EditText etAge = findViewById(R.id.etAge);
        EditText etEspece = findViewById(R.id.etEspece);

        if (view.getId() == R.id.btEnvoi) {
            if (etNom.getText().toString().isEmpty() ||
                    etAge.getText().toString().isEmpty() ||
                    etEspece.getText().toString().isEmpty()) {
                Toast.makeText(this, getString(R.string.animal_invalide),
                        Toast.LENGTH_LONG).show();
            } else {
                String nom = etNom.getText().toString();
                int age = Integer.parseInt(etAge.getText().toString());
                String espece = etEspece.getText().toString();
                Log.i("AnimalActivity", "Nouvel animal : " + nom + " " + age + " " + espece);

                ZooHelper zooHelper = new ZooHelper(this);
                int nb = zooHelper.insererAnimal(nom, age, espece);
                Log.i("AnimalActivity", "C'est le " + espece + " numero : " + nb);
                List<String> animaux = zooHelper.getAnimaux();
                for (String animal : animaux)
                    Log.i("AnimalActivity", animal);

                etNom.setText("");
                etAge.setText("");
                etEspece.setText("");
            }
        }

        if (view.getId() == R.id.btChercher) {
            if(checkSelfPermission(Manifest.permission.INTERNET)==
                    PackageManager.PERMISSION_GRANTED) {
                ExecutorService executorService = Executors.newFixedThreadPool(2);
                // non, NetworkOnMainThread - chercher();
                executorService.execute(this::chercher);
            } else {
                requestPermissions(new String[] {
                        Manifest.permission.INTERNET
                }, 0);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            ExecutorService executorService = Executors.newFixedThreadPool(2);
            executorService.execute(this::chercher);
        } else {
            Log.i("AnimalActivity", "L'usager refuse l'accès à internet");
        }
    }

    public void chercher() {
        try {
            afficher("...");
            EditText etEspece = findViewById(R.id.etEspece);
            String espece = etEspece.getText().toString();
            String url = "https://fr.wikipedia.org/w/api.php?action=query&" +
                    "prop=extracts&exsentences=3&format=json&titles=" +
                    URLEncoder.encode(espece, "UTF-8");
            Log.i("AnimalActivity", "Recherche de : " + url);
            URLConnection conn = new URL(url).openConnection();
            InputStream is = conn.getInputStream();
            // TODO lire le flux, ecrire dans la TextView
            String contenuJson = "";
            String ligne;
            BufferedReader br = new BufferedReader(new
                    InputStreamReader(is, StandardCharsets.UTF_8));
            while((ligne = br.readLine())!=null)
                contenuJson += ligne;
            br.close();
            is.close();
            JSONObject racine = new JSONObject(contenuJson);
            JSONObject query = racine.getJSONObject("query");
            JSONObject pages = query.getJSONObject("pages");
            String numeroPage = pages.keys().next();
            JSONObject page = pages.getJSONObject(numeroPage);
            String contenuHtml = page.getString("extract");

            afficher(Html.fromHtml(contenuHtml));
        } catch(Exception ex) {
            Log.e("AnimalActivity", "Echec recherche", ex);
        }
    }

    private void afficher(CharSequence message) {
        mainHandler.post(() -> {
            TextView tvResultat = findViewById(R.id.tvResultat);
            tvResultat.setText(message);
        });
    }
}
