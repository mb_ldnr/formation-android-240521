package fr.ldnr.mickael.monzoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class PopcornActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new PopcornView(this));
        Log.i("PopcornActivity", "activité créée");
        long duree = getIntent() .getLongExtra("temps", 0);
        if(duree > 500) {

            String secondes = getResources().getQuantityString(
                    R.plurals.popcorn_secondes, (int)duree/1000
            );
            String texte = getString( R.string.popcorn_avertissement,
                    (int)duree/1000, secondes );

            Toast.makeText(this, texte, Toast.LENGTH_LONG).show();
            Intent resultat = new Intent();
            resultat.putExtra(AquariumActivity.CLE_RETOUR, true);
            setResult(0, resultat);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked()==MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, CarteActivity.class);
            //i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            // Si la carte est déjà dessous, la reprendre
            //i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            // ??
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        return true;
    }

    public class PopcornView extends View {

        public PopcornView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(@NonNull Canvas canvas) {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),
                    R.drawable.popcorn);
            canvas.drawBitmap(bmp, 0, 0, null);
        }
    }
}
